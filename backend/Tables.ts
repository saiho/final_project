const tables = Object.freeze({
  BOOKMARKS: "bookmarks",
  COMPLETIONS: "completions",
  LIKES: "likes",
  RECIPES: "recipes",
  USERS: "users",
});

export default tables;
