import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import express from "express";
import jwt from "./jwt";
import { UserService } from "./services/UserService";
import { User } from "./models";
// import { userService } from './main';
// import { User } from './services/models';

const permit = new Bearer({
  query: "access_token",
});

export function isLoggedIn(userService: UserService) {
  return async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    try {
      const token = permit.check(req);
      if (!token) {
        return res.status(401).json({ msg: "Permission Denied" });
      }
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const user: User = await userService.getUserByUsername(payload.username);
      if (user) {
        req.user = user;
        return next();
      } else {
        return res.status(401).json({ msg: "Permission Denied" });
      }
    } catch (e) {
      return res.status(401).json({ msg: "Permission Denied" });
    }
  };
}
