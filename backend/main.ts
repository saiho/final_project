import express from "express";
import bodyParser from "body-parser";
import Knex from "knex";
import config from "./knexfile";

import { RecipeService } from "./services/RecipeService";
import { RecipeRouter } from "./routers/RecipeRouter";
import { UserService } from "./services/UserService";
import { AuthRouter } from "./routers/AuthRouter";
import { UserRouter } from "./routers/UserRouter";
import { isLoggedIn } from "./guard";

const knex = Knex(config["development"]);
const recipeService = new RecipeService(knex);
const recipeRouter = new RecipeRouter(recipeService);

const userService = new UserService(knex);
const authRouter = new AuthRouter(userService);
const userRouter = new UserRouter(userService);
export const isLoggedInGuard = isLoggedIn(userService);

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static("public"));

const API_VERSION = "/api";
app.use(API_VERSION, authRouter.router());
app.use(API_VERSION, userRouter.router());
app.use(API_VERSION, isLoggedInGuard, recipeRouter.router());

app.listen(process.env.PORT, () => {
  console.log("Listing on PORT " + process.env.PORT);
});
