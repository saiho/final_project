import * as Knex from "knex";
import tables from "../Tables";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tables.USERS, (table) => {
    table.increments();
    table.string("username").notNullable();
    table.string("password").notNullable();
    table.string("firstname").notNullable();
    table.string("lastname").notNullable();
    table.string("avatar");
    table.integer("level").unsigned();
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tables.RECIPES, (table) => {
    table.increments();
    table.integer("owner_id").notNullable();
    table.string("picture").notNullable();
    table.string("title").notNullable();
    table.text("content").notNullable();
    table.timestamps(false, true);

    table.foreign("owner_id").references("users.id");
  });

  await knex.schema.createTable(tables.COMPLETIONS, (table) => {
    table.increments();
    table.integer("recipe_id").unsigned().notNullable();
    table.string("picture").notNullable();
    table.integer("user_id").unsigned().notNullable();
    table.text("comment");
    table.timestamps(false, true);

    table.foreign("recipe_id").references("recipes.id");
    table.foreign("user_id").references("users.id");
  });

  await knex.schema.createTable(tables.BOOKMARKS, (table) => {
    table.increments();
    table.integer("recipe_id").unsigned().notNullable();
    table.integer("user_id").unsigned().notNullable();
    table.timestamps(false, true);

    table.foreign("recipe_id").references("recipes.id");
    table.foreign("user_id").references("users.id");
  });

  await knex.schema.createTable(tables.LIKES, (table) => {
    table.increments();
    table.integer("completion_id").unsigned().notNullable();
    table.integer("user_id").unsigned().notNullable();
    table.timestamps(false, true);

    table.foreign("completion_id").references("completions.id");
    table.foreign("user_id").references("users.id");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists(tables.LIKES);
  await knex.schema.dropTableIfExists(tables.BOOKMARKS);
  await knex.schema.dropTableIfExists(tables.COMPLETIONS);
  await knex.schema.dropTableIfExists(tables.RECIPES);
  await knex.schema.dropTableIfExists(tables.USERS);
}
