export interface User {
  id: number;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  avatar: string;
  level: number;
  created_at: string;
  updated_at: string;
}

declare global {
  namespace Express {
    interface Request {
      user?: User;
    }
  }
}
