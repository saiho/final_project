import { Bearer } from "permit";
import express from "express";
import jwt from "../jwt";
import jwtSimple from "jwt-simple";

import { checkPassword } from "../hash";
import { UserService } from "../services/UserService";

const permit = new Bearer({
  query: "access_token",
});

export class AuthRouter {
  constructor(private userService: UserService) {}

  router() {
    const router = express.Router();
    router.post("/login", this.login);
    router.get("/current_user", this.validateUser);
    return router;
  }

  private login = async (req: express.Request, res: express.Response) => {
    try {
      if (!req.body.username || !req.body.password) {
        res.status(401).json({ message: "Invalid username/password" });
        return;
      }
      const { username, password } = req.body;
      const user = await this.userService.getUserByUsername(username);
      if (!user || !(await checkPassword(password, user.password))) {
        res.status(401).json({ message: "Invalid username/password" });
        return;
      }
      const payload = {
        id: user.id,
        username: user.username,
      };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);
      res.json({
        token: token,
      });
    } catch (e) {
      console.log(e);
      res.status(500).json({ msg: e.toString() });
    }
  };

  private validateUser = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      // retrieve token
      const token = permit.check(req);
      if (!token) {
        res.status(401).json({ message: "Permission Denied" });
      }
      // decode token
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      // found user from the decode token id
      const user = await this.userService.getUserByUsername(payload.username);

      if (!user) {
        res.status(401).json({ msg: "Permission Denied" });
      }
      // if found let go
      const { password, ...others } = user;
      res.json({ ...others });
    } catch (err) {
      res.status(401).json({ msg: "Permission Denied" });
    }
  };
}
