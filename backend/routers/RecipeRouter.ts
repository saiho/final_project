import express from "express";
import fetch from "node-fetch";
import { RecipeService } from "../services/RecipeService";
import { upload } from "../multer";

export class RecipeRouter {
  constructor(private recipeService: RecipeService) { }

  router() {
    const router = express.Router();
    router.get("/recipes", this.getAllRecipes);
    router.post("/recipes/search", this.getRecipes);
    router.post("/recipes/bookmark/search/:user_id", this.getSearchBookmarks);
    router.post("/completions/search", this.getSearchCompletions);
    router.post("/recipe/generate", upload.single("image"), this.generateNewRecipe);
    router.post("/recipe", this.addNewRecipe);
    router.post("/recipe/bookmark", this.addBookmark);
    router.delete("/recipe/bookmark", this.removeBookmark);
    router.get("/recipe/is_bookmark/:user_id/:recipe_id", this.isBookmark);
    router.get("/recipe/is_completed/:user_id/:recipe_id", this.isCompleted);
    router.get("/recipes/bookmark/:user_id", this.getBookmarkedRecipes);
    router.post("/recipe/comment", upload.single("image"), this.addComment);
    router.get("/recipe/completions/:recipe_id", this.getCompletions);
    router.get("/recipe/completion/is_like/:completion_id/:user_id", this.isLike);
    router.post("/recipe/completion/like/:completion_id/:user_id", this.addLike);
    router.delete("/recipe/completion/like/:completion_id/:user_id", this.removeLike);
    router.get("/recipe/completion/num_likes/:completion_id", this.getNumOfLikes);
    router.get("/recipe/completions", this.getAllCompletions);
    return router;
  }

  private getAllRecipes = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const recipes = await this.recipeService.getRecipes();
      for (let recipe of recipes) {
        const completions = await this.recipeService.getNumOfCompletion(
          recipe.id
        );
        recipe["num_completions"] = completions!.count;
      }
      res.json(recipes);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private getRecipes = async (req: express.Request, res: express.Response) => {
    try {
      let { query } = req.body;
      query = query.toLowerCase();
      let queries: Array<string> = query.split(" ");
      queries = queries.filter((q) => q != "");
      let temp = queries[0];
      for (let i = 1; i < queries.length; i++) temp += `|${queries[i]}`;
      const regExp = new RegExp(temp);

      const allRecipes = await this.recipeService.getRecipes();
      for (let recipe of allRecipes) {
        const completions = await this.recipeService.getNumOfCompletion(
          recipe.id
        );
        recipe["num_completions"] = completions!.count;
      }

      const recipes = allRecipes.filter(
        (recipe) =>
          regExp.test(recipe.content.toLowerCase()) ||
          regExp.test(recipe.title.toLowerCase())
      );
      res.json(recipes);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private getSearchBookmarks = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      let { query } = req.body;
      query = query.toLowerCase();
      let queries: Array<string> = query.split(" ");
      queries = queries.filter((q) => q != "");
      let temp = queries[0];
      for (let i = 1; i < queries.length; i++) temp += `|${queries[i]}`;
      const regExp = new RegExp(temp);

      const { user_id } = req.params;

      const allBookmarks = await this.recipeService.getBookmarkedRecipes(
        parseInt(user_id)
      );
      for (let recipe of allBookmarks) {
        const completions = await this.recipeService.getNumOfCompletion(
          recipe.id
        );
        recipe["num_completions"] = completions!.count;
      }

      const recipes = allBookmarks.filter(
        (recipe) =>
          regExp.test(recipe.content.toLowerCase()) ||
          regExp.test(recipe.title.toLowerCase())
      );
      res.json(recipes);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private getSearchCompletions = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      let { query } = req.body;
      query = query.toLowerCase();
      let queries: Array<string> = query.split(" ");
      queries = queries.filter((q) => q != "");
      let temp = queries[0];
      for (let i = 1; i < queries.length; i++) temp += `|${queries[i]}`;
      const regExp = new RegExp(temp);

      const allCompletions = await this.recipeService.getAllCompletions();

      for (const completion of allCompletions) {
        const num_likes = await this.recipeService.getNumOfLikes(
          completion.id
        );
        completion["num_likes"] = num_likes!.count;
      }

      const completions = allCompletions.filter(
        (completion) =>
          regExp.test(completion.title.toLowerCase()) ||
          regExp.test(completion.comment.toLowerCase())
      );
      res.json(completions);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private generateNewRecipe = async (
    req: express.Request,
    res: express.Response
  ) => {
    const image_url = `${process.env.HOST}/uploads/${req.file.filename}`;
    try {
      const data = await fetch(`http://localhost:5000`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(image_url),
      });

      let recipes: Array<any>;
      recipes = await data.json();
      recipes.map((recipe) => (recipe["picture"] = image_url));
      res.json(recipes);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private addBookmark = async (req: express.Request, res: express.Response) => {
    try {
      const recipe_id = parseInt(req.body.recipe_id);
      const user_id = parseInt(req.body.user_id);
      await this.recipeService.addBookmark(recipe_id, user_id);
      res.json({ message: "Bookmark is added." });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private removeBookmark = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const recipe_id = parseInt(req.body.recipe_id);
      const user_id = parseInt(req.body.user_id);
      await this.recipeService.removeBookmark(recipe_id, user_id);
      res.json({ message: "Bookmark is removed." });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private addLike = async (req: express.Request, res: express.Response) => {
    try {
      const { completion_id, user_id } = req.params;
      await this.recipeService.addLike(
        parseInt(completion_id),
        parseInt(user_id)
      );
      res.json({ message: "Like is added." });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private removeLike = async (req: express.Request, res: express.Response) => {
    try {
      const { completion_id, user_id } = req.params;
      await this.recipeService.removeLike(
        parseInt(completion_id),
        parseInt(user_id)
      );
      res.json({ message: "Like is removed." });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private isBookmark = async (req: express.Request, res: express.Response) => {
    try {
      const { user_id, recipe_id } = req.params;
      const hasBookmark = await this.recipeService.isBookmark(
        parseInt(recipe_id),
        parseInt(user_id)
      );
      res.json({ is_bookmark: hasBookmark ? true : false });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private isCompleted = async (req: express.Request, res: express.Response) => {
    try {
      const { user_id, recipe_id } = req.params;
      const hasCompleted = await this.recipeService.isCompleted(
        parseInt(recipe_id),
        parseInt(user_id)
      );
      res.json({ is_completed: hasCompleted ? true : false });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private isLike = async (req: express.Request, res: express.Response) => {
    try {
      const { completion_id, user_id } = req.params;
      const hasLike = await this.recipeService.isLike(
        parseInt(completion_id),
        parseInt(user_id)
      );
      res.json({ is_like: hasLike ? true : false });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private getBookmarkedRecipes = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const { user_id } = req.params;
      const recipes = await this.recipeService.getBookmarkedRecipes(
        parseInt(user_id)
      );
      for (let recipe of recipes) {
        const completions = await this.recipeService.getNumOfCompletion(
          recipe.id
        );
        recipe["num_completions"] = completions!.count;
      }
      res.json(recipes);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  private addNewRecipe = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const recipe = req.body;
      const { owner_id, picture, title } = req.body;
      const _owner_id = parseInt(owner_id);

      const ingredients = recipe.ingredients.reduce(
        (acc: any, current: any) => acc + current + `, `,
        "Ingredients: \n"
      );

      const instructions = recipe.instructions.reduce(
        (acc: any, current: any) => acc + `- ` + current + `\n`,
        "Instructions: \n"
      );

      const content =
        ingredients.slice(0, ingredients.length - 2) + "\n\n" + instructions;

      await this.recipeService.addNewRecipe(content, _owner_id, picture, title);

      res.json({ message: "New rccipe get" });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  public addComment = async (req: express.Request, res: express.Response) => {
    try {
      const picture = `${process.env.HOST}/uploads/${req.file.filename}`;
      const { user_id, recipe_id, comment } = req.body;

      await this.recipeService.addComment(
        comment,
        picture,
        parseInt(recipe_id),
        parseInt(user_id)
      );
      res.json({ message: "Comment is added." });
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  public getCompletions = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const { recipe_id } = req.params;
      const completions = await this.recipeService.getCompletions(
        parseInt(recipe_id)
      );
      res.json(completions);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  public getNumOfLikes = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const { completion_id } = req.params;
      const numOfLikes = await this.recipeService.getNumOfLikes(
        parseInt(completion_id)
      );
      res.json(numOfLikes);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };

  public getAllCompletions = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      const completions = await this.recipeService.getAllCompletions();

      for (const completion of completions) {
        const num_likes = await this.recipeService.getNumOfLikes(completion.id);
        completion["num_likes"] = num_likes!.count;
      }
      res.json(completions);
    } catch (error) {
      console.log(error);
      res.json({ message: "Internal server error" });
    }
  };
}
