import express from "express";
import { UserService } from "../services/UserService";
import { upload } from "../multer";
import { hashPassword } from "../hash";
import { isLoggedInGuard } from "../main";

export class UserRouter {
  constructor(private userService: UserService) {}

  router() {
    const router = express.Router();
    router.post("/register", upload.single("avatar"), this.register);
    router.get("/user/:id", isLoggedInGuard, this.getUser);
    router.get(
      "/user/num_completions/:user_id",
      isLoggedInGuard,
      this.getNumOfCompletions
    );
    router.get(
      "/user/num_recipes/:owner_id",
      isLoggedInGuard,
      this.getNumOfRecipes
    );
    router.get(
      "/user/num_likes/:user_id",
      isLoggedInGuard,
      this.getTotalNumOfLikes
    );
    router.get(
      "/user/completions/:user_id",
      isLoggedInGuard,
      this.getUserCompletions
    );
    return router;
  }

  private register = async (req: express.Request, res: express.Response) => {
    const filename = req.file ? req.file.filename : "";
    const { firstname, lastname, email, password } = req.body;
    const user = await this.userService.getUserByUsername(email);

    if (user) {
      res
        .status(401)
        .json({ message: "Username has been registered already." });
      return;
    }
    const hashpassword = await hashPassword(password);
    try {
      await this.userService.register(
        firstname,
        lastname,
        email,
        hashpassword,
        filename
      );
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
    return res.json({ message: "User has registered successfully." });
  };

  private getUser = async (req: express.Request, res: express.Response) => {
    const { id } = req.params;

    try {
      const response = await this.userService.getUserByID(parseInt(id));
      const { password, ...others } = response;
      res.json({ ...others });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  private getNumOfCompletions = async (
    req: express.Request,
    res: express.Response
  ) => {
    const { user_id } = req.params;

    try {
      const num = await this.userService.getNumOfCompletions(parseInt(user_id));
      res.json(num);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  private getUserCompletions = async (
    req: express.Request,
    res: express.Response
  ) => {
    const { user_id } = req.params;

    try {
      const completions = await this.userService.getUserCompletions(
        parseInt(user_id)
      );

      for (let completion of completions) {
        const numOfLike = await this.userService.getNumOfLike(completion.id);
        completion["numOfLike"] = numOfLike!.count;
      }
      res.json(completions);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  private getNumOfRecipes = async (
    req: express.Request,
    res: express.Response
  ) => {
    const { owner_id } = req.params;
    try {
      const num = await this.userService.getNumOfRecipes(parseInt(owner_id));
      res.json(num);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  private getTotalNumOfLikes = async (
    req: express.Request,
    res: express.Response
  ) => {
    const { user_id } = req.params;
    try {
      const num = await this.userService.getTotalNumOfLikes(parseInt(user_id));
      res.json(num);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "Internal server error" });
    }
  };
}
