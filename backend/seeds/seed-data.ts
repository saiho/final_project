import * as Knex from "knex";
import tables from "../Tables";
import { hashPassword } from "../hash";
import * as dotenv from "dotenv";
dotenv.config();

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(tables.LIKES).del();
  await knex(tables.BOOKMARKS).del();
  await knex(tables.COMPLETIONS).del();
  await knex(tables.RECIPES).del();
  await knex(tables.USERS).del();

  const password = await hashPassword("1234");
  // Inserts seed entries
  await knex(tables.USERS).insert([
    {
      username: "saiho@gmail.com",
      password: password,
      firstname: "Saiho",
      lastname: "Ip",
      level: 0,
      avatar:
        "https://images.goodsmile.info/cgm/images/product/20200501/9477/69452/large/173896bf77b01889ef0cfb6a6108c190.jpg",
    },
    {
      username: "jason@gmail.com",
      password: password,
      firstname: "Jason",
      lastname: "Yip",
      level: 0,
      avatar: "https://data.gamertb.com/data/ms/images/b/4605.png",
    },
    {
      username: "alex@tecky.io",
      password: password,
      firstname: "Alex",
      lastname: "Lau",
      level: 0,
      avatar:
        "https://tecky.io/static/5d3fe7bec531cDI8A4077-7703e771e319f3bd8d6fc4c5156f08e9.JPG",
    },
    {
      username: "gordon@tecky.io",
      password: password,
      firstname: "Gordon",
      lastname: "Lau",
      level: 0,
      avatar:
        "https://tecky.io/static/5e3ada50ca689suit-3fdc87b2ef04555b3e82ed7d6c4acfd5.jpg",
    },
    {
      username: "jason@tecky.io",
      password: password,
      firstname: "Jason",
      lastname: "Li",
      level: 0,
      avatar:
        "https://tecky.io/static/5d4bdeff202af3ad0f5b8-02fe-4b54-a8cf-73dd7a945436-95bd3a2a104d76a034239e2356fb83b3.jpg",
    },
  ]);

  const id1 = (
    await knex(tables.USERS)
      .select("id")
      .where("username", "=", "saiho@gmail.com")
      .first()
  ).id;

  const id2 = (
    await knex(tables.USERS)
      .select("id")
      .where("username", "=", "jason@gmail.com")
      .first()
  ).id;

  const id3 = (
    await knex(tables.USERS)
      .select("id")
      .where("username", "=", "alex@tecky.io")
      .first()
  ).id;

  const id4 = (
    await knex(tables.USERS)
      .select("id")
      .where("username", "=", "gordon@tecky.io")
      .first()
  ).id;

  const id5 = (
    await knex(tables.USERS)
      .select("id")
      .where("username", "=", "jason@tecky.io")
      .first()
  ).id;

  await knex(tables.RECIPES).insert([
    {
      owner_id: id1,
      picture: `https://food.fnr.sndimg.com/content/dam/images/food/fullset/2009/4/5/1/BW2D10_28652_s4x3.jpg.rend.hgtvcom.826.620.suffix/1431856622401.jpeg`,
      title: "Avocado egg salad sandwich",
      content: `Ingredients:
avocado, bread, tomato, mayonnaise, pepper, egg, salt
\nInstructions:
- Toast bread.
- Spread mayonnaise on one side of each slice.
- Place avocado slices on one slice of bread.
- Top with tomato slices and salt and pepper.
- Top with other slice of bread.`,
    },
    {
      owner_id: id2,
      picture: `https://healthyfitnessmeals.com/wp-content/uploads/2020/05/instagram-In-Stream_Square___Shrimp-spaghetti-3.jpg`,
      title: "Garlic shrimp pasta",
      content: `Ingredients:
shrimp, pepper, butter, clove, oil, salt, pasta, parsley
\nInstructions:
- Cook pasta according to package directions, omitting salt and fat.
- Add butter and cook for 1 minute or until butter is melted.
- Add shrimp, garlic, and salt and pepper; cook 3 minutes, stirring constantly.
- Drain pasta and add to shrimp mixture; toss gently to coat.
- Sprinkle with parsley.`,
    },
    {
      owner_id: id3,
      picture: `https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fcdn-image.foodandwine.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2F2014-r-xl-grilled-eggplant-and-zucchini-salad-with-salsa-verde.jpg%3Fitok%3DJukRUbyW`,
      title: "Grilled eggplant and zucchini",
      content: `Ingredients:
zucchini, oil, salt, squash, pepper, eggplant
\nInstructions:
- Preheat grill to medium-high.
- Brush eggplant slices with oil and season with salt and pepper.
- Grill, turning once, until tender and lightly charred, about 10 minutes.
- Transfer to a platter and let cool.
- Cut into 1/2-inch slices.
- Serve with grilled zucchini.`,
    },
    {
      owner_id: id4,
      picture: `https://www.unclejerryskitchen.com/wp-content/uploads/2009/07/French-Toast-with-Homemade-Blueberry-Syrup.jpg`,
      title: "Blueberry french toast",
      content: `Ingredients: 
egg, bread, sugar, milk, cinnamon, butter, syrup, blueberries, extract, vanilla, salt
\nInstructions: 
- Beat eggs, milk, vanilla, salt, sugar, and cinnamon together in a shallow glass dish.
- Dip bread slices in egg mixture, then place in refrigerator to soak until ready to cook.
- Melt butter in a large skillet over medium heat.
- When butter begins to melt, place half of the soaked bread slices into the skillet and cook until golden brown on both sides, 2 to 3 minutes per side.
- Repeat with remaining bread slices.`,
    },
    {
      owner_id: id5,
      picture: `https://hips.hearstapps.com/hmg-prod/images/delish-slow-cooker-chicken-tikka-masala-2-copy-1535578244.jpg`,
      title: "Chicken tikka masala",
      content: `Ingredients: 
onion, oil, chicken, pepper, tomato, clove, salt, curry, ginger, turmeric, flour
\nInstructions: 
- Combine flour, salt and spices and dredge chicken in mixture to coat.
- Heat olive oil in skillet over medium heat.
- Brown chicken on all sides.
- Reduce heat to medium and add onion, tomatoes and garlic.
- Stir until onions are tender.
- Mix in the chicken.
- Cook for a few minutes.
- Add the curry.
- Mix well so that the chicken absorbs all the sauce.
- Cover and simmer for 20 minutes until chicken is cooked through.`,
    },
    {
      owner_id: id1,
      picture: `https://foodexpress.com.ng/wp-content/uploads/2020/05/Pizza-Vegetariana-2.jpeg`,
      title: "Pizza with roasted tomatoes, onions, and olives",
      content: `Ingredients: 
tomato, cheese, onion, olive, basil, pepper, oil, pizza_crust, clove
\nInstructions: 
- Preheat oven to 450 degrees f.
- Place pizza crust on baking sheet.
- Brush tomatoes, onions, garlic, olive oil, salt, and pepper over crust.
- Sprinkle basil evenly over tomatoes.
- Roast 15 minutes.
- Top pizza with mozzarella cheese.
- Bake 10 minutes more, or until cheese is melted and crust is golden brown.`,
    },
  ]);
}
