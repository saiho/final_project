import Knex from "knex";

import tables from "../Tables";

export class RecipeService {
  constructor(private knex: Knex) {}

  public getRecipes = async () => {
    const recipes = await this.knex
      .select(
        "recipes.id as id",
        "owner_id",
        "recipes.picture as picture",
        "title",
        "content",
        "recipes.created_at as created_at",
        "username",
        "firstname",
        "lastname",
        "level",
        "avatar"
      )
      .from(tables.RECIPES)
      .join("users", "users.id", "recipes.owner_id")
      .orderBy("recipes.created_at", "desc");

    return recipes;
  };

  public getAllCompletions = async () => {
    const completions = await this.knex
      .select(
        "completions.id",
        "recipe_id",
        "completions.picture",
        "user_id",
        "comment",
        "completions.created_at as created_at",
        "users.username",
        "users.firstname",
        "users.lastname",
        "users.level",
        "users.avatar",
        "recipes.title"
      )
      .from(tables.COMPLETIONS)
      .join("users", "users.id", "completions.user_id")
      .join("recipes", "recipes.id", "completions.recipe_id")
      .orderBy("recipes.created_at", "desc");

    return completions;
  };

  public getCompletionById = async (completion_id: number) => {
    const completion = await this.knex
      .select(
        "completions.id",
        "recipe_id",
        "completions.picture",
        "user_id",
        "comment",
        "completions.created_at as created_at",
        "users.username",
        "users.firstname",
        "users.lastname",
        "users.level",
        "users.avatar",
        "recipes.title"
      )
      .from(tables.COMPLETIONS)
      .join("users", "users.id", "completions.user_id")
      .join("recipes", "recipes.id", "completions.recipe_id")
      .where("completions.id", completion_id)
      .first();

    return completion;
  };

  public addBookmark = async (recipe_id: number, user_id: number) => {
    await this.knex(tables.BOOKMARKS).insert({ recipe_id, user_id });
  };

  public removeBookmark = async (recipe_id: number, user_id: number) => {
    await this.knex(tables.BOOKMARKS)
      .del()
      .where("recipe_id", recipe_id)
      .andWhere("user_id", user_id);
  };

  public addLike = async (completion_id: number, user_id: number) => {
    await this.knex(tables.LIKES).insert({ completion_id, user_id });
  };

  public removeLike = async (completion_id: number, user_id: number) => {
    await this.knex(tables.LIKES)
      .del()
      .where("completion_id", completion_id)
      .andWhere("user_id", user_id);
  };

  public isBookmark = async (recipe_id: number, user_id: number) => {
    return await this.knex(tables.BOOKMARKS)
      .select("*")
      .where("recipe_id", recipe_id)
      .andWhere("user_id", user_id)
      .first();
  };

  public isLike = async (completion_id: number, user_id: number) => {
    return await this.knex(tables.LIKES)
      .select("*")
      .where("completion_id", completion_id)
      .andWhere("user_id", user_id)
      .first();
  };

  public isCompleted = async (recipe_id: number, user_id: number) => {
    return await this.knex(tables.COMPLETIONS)
      .select("*")
      .where("recipe_id", recipe_id)
      .andWhere("user_id", user_id)
      .first();
  };

  public getBookmarkedRecipes = async (user_id: number) => {
    const recipes = await this.knex
      .select(
        "recipes.id as id",
        "owner_id",
        "recipes.picture as picture",
        "title",
        "content",
        "recipes.created_at as created_at",
        "username",
        "firstname",
        "lastname",
        "level",
        "avatar"
      )
      .from(tables.RECIPES)
      .join("users", "users.id", "recipes.owner_id")
      .join("bookmarks", "bookmarks.recipe_id", "recipes.id")
      .where("bookmarks.user_id", user_id)
      .orderBy("recipes.created_at", "desc");

    return recipes;
  };

  public addNewRecipe = async (
    content: string,
    owner_id: number,
    picture: string,
    title: string
  ) => {
    await this.knex(tables.RECIPES).insert({
      content,
      owner_id,
      picture,
      title,
    });
  };

  public addComment = async (
    comment: string,
    picture: string,
    recipe_id: number,
    user_id: number
  ) => {
    await this.knex(tables.COMPLETIONS).insert({
      comment,
      picture,
      recipe_id,
      user_id,
    });
  };

  public getCompletions = async (id: number) => {
    return await this.knex(tables.COMPLETIONS).where("recipe_id", id);
  };

  public getNumOfCompletion = async (recipe_id: number) => {
    return await this.knex(tables.COMPLETIONS)
      .count("id")
      .where("recipe_id", recipe_id)
      .first();
  };

  public getNumOfLikes = async (completion_id: number) => {
    return await this.knex(tables.LIKES)
      .count("id")
      .where("completion_id", completion_id)
      .first();
  };
}
