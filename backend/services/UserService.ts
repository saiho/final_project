import Knex from "knex";
import tables from "../Tables";

export class UserService {
  constructor(private knex: Knex) {}

  getUserByUsername = async (username: string) => {
    const user = await this.knex(tables.USERS)
      .where("username", username)
      .first();
    return user;
  };

  getUserByID = async (id: number) => {
    const user = await this.knex(tables.USERS).where("id", id).first();
    return user;
  };

  register = async (
    firstname: string,
    lastname: string,
    username: string,
    password: string,
    picture: string
  ) => {
    if (picture == "")
      await this.knex(tables.USERS).insert({
        firstname,
        lastname,
        username,
        password,
      });
    else
      await this.knex(tables.USERS).insert({
        firstname,
        lastname,
        username,
        password,
        avatar: `${process.env.HOST}/uploads/${picture}`,
      });
  };

  getNumOfCompletions = async (user_id: number) => {
    return await this.knex(tables.COMPLETIONS)
      .count("id")
      .where("user_id", user_id)
      .first();
  };

  getUserCompletions = async (user_id: number) => {
    return await this.knex(tables.COMPLETIONS)
      .select(
        "completions.id",
        "completions.picture",
        "completions.comment",
        "completions.created_at",
        "recipes.title"
      )
      .join("recipes", "recipes.id", "completions.recipe_id")
      .where("completions.user_id", user_id)
      .orderBy("completions.created_at", "desc");
  };

  getNumOfRecipes = async (owner_id: number) => {
    return await this.knex(tables.RECIPES)
      .count("id")
      .where("owner_id", owner_id)
      .first();
  };

  getNumOfLike = async (completion_id: number) => {
    return await this.knex(tables.LIKES)
      .count("id")
      .where("completion_id", completion_id)
      .first();
  };

  getTotalNumOfLikes = async (user_id: number) => {
    return await this.knex(tables.COMPLETIONS)
      .count("likes.id")
      .join("likes", "likes.completion_id", "completions.id")
      .where("completions.user_id", user_id)
      .first();
  };
}
