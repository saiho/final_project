import React, {useEffect, useState} from 'react';

import AuthContext from './app/auth/context';
import AuthNavigator from './app/navigation/AuthNavigator';
import authStorage from './app/auth/store';
import AppNavigator from './app/navigation/AppNavigator';
import {HOST} from './app/configs/host';
import {NavigationContainer} from '@react-navigation/native';
import navigationTheme from './app/navigation/navigationTheme';

const App: () => React$Node = () => {
  const [user, setUser] = useState();

  const restoreUser = async () => {
    const token = await authStorage.getToken();
    if (!token) return;

    const userRes = await fetch(`${HOST}/current_user`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (userRes.status !== 200) return;
    const user = await userRes.json();
    setUser(user);
  };

  useEffect(() => {
    restoreUser();
  }, []);

  return (
    <AuthContext.Provider value={{user, setUser}}>
      <NavigationContainer theme={navigationTheme}>
        {user ? <AppNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;
