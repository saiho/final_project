import RNSecureStorage, {ACCESSIBLE} from 'rn-secure-storage';

const key = 'authToken';

const storeToken = async (authToken) => {
  try {
    await RNSecureStorage.set(key, authToken, {
      accessible: ACCESSIBLE.WHEN_UNLOCKED,
    });
  } catch (error) {
    console.log('Error occurs on storing the auth token.', error);
  }
};

const getToken = async () => {
  try {
    return await RNSecureStorage.get(key);
  } catch (error) {
    console.log('Error occurs on getting the auth token.', error);
  }
};

const removeToken = async () => {
  try {
    await RNSecureStorage.remove(key);
  } catch (error) {
    console.log('Error occurs on removing the auth token.', error);
  }
};

export default {storeToken, getToken, removeToken};
