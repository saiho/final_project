import React from 'react';
import LottieView from 'lottie-react-native';

function ActivityIndicator({visible = false, isRecipe = false}) {
  if (!visible) {
    return null;
  } else if (isRecipe) {
    return (
      <LottieView
        autoPlay
        loop
        source={require('../assets/animations/recipe_loader.json')}
      />
    );
  } else if (!isRecipe) {
    return (
      <LottieView
        autoPlay
        loop
        source={require('../assets/animations/loader.json')}
      />
    );
  }
}
export default ActivityIndicator;
