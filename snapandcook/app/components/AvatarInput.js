import React from 'react';
import {Image, StyleSheet, View, Text} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Alert} from 'react-native';
import colors from '../configs/colors';

export default function AvatarInput({imageUri, onChangeImage}) {
  const handlePress = () => {
    if (!imageUri) selectImage();
    else
      Alert.alert('Delete', 'Are you sure to delete this image?', [
        {text: 'Yes', onPress: () => onChangeImage(null)},
        {text: 'No'},
      ]);
  };

  const selectImage = () => {
    try {
      ImagePicker.showImagePicker(
        {
          mediaType: 'photo',
          quality: 0.6,
        },
        (response) => {
          if (!response.didCancel) onChangeImage(response.uri);
        },
      );
    } catch (error) {
      console.log('Error reading an image', error);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      <View style={styles.container}>
        {!imageUri && (
          <MaterialCommunityIcons
            color={colors.medium}
            name="account-plus"
            size={80}
          />
        )}
        {imageUri && <Image source={{uri: imageUri}} style={styles.image} />}
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.light,
    borderRadius: 100,
    height: 120,
    justifyContent: 'center',
    overflow: 'hidden',
    width: 120,
  },
  image: {
    height: '100%',
    width: '100%',
  },
});
