import React from 'react';
import {Dimensions, Image, StyleSheet, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

import Text from '../components/Text';

export default function GridItems({items}) {
  const navigation = useNavigation();

  return (
    <>
      <Text style={styles.title}>All Creations</Text>
      <View style={styles.container}>
        {items.map((item) => {
          return (
            <TouchableOpacity
              key={item.id}
              onPress={() => {
                navigation.navigate('Creation', item);
              }}>
              <View style={styles.item}>
                <Image style={styles.image} source={{uri: item.picture}} />
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingBottom: 50,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  item: {
    borderRadius: 15,
    height: (Dimensions.get('window').width - 80) / 4,
    marginRight: 10,
    overflow: 'hidden',
    width: (Dimensions.get('window').width - 80) / 4,
  },
  title: {
    fontSize: 24,
    paddingBottom: 10,
  },
});
