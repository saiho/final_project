import {Dimensions, ScrollView, StyleSheet, View} from 'react-native';
import React, {useContext} from 'react';

import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import colors from '../configs/colors';
import {HOST} from '../configs/host';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Text from '../components/Text';
import {useNavigation} from '@react-navigation/native';

export default function RecipeCard({recipe}) {
  const {user} = useContext(AuthContext);
  const navigation = useNavigation();

  const ingredients = recipe.ingredients.reduce(
    (acc, current) =>
      acc + `- ` + current.charAt(0).toUpperCase() + current.slice(1) + `\n`,
    '',
  );
  const instructions = recipe.instructions.reduce(
    (acc, current) =>
      acc + `- ` + current.charAt(0).toUpperCase() + current.slice(1) + `\n`,
    '',
  );

  const handleUpload = async () => {
    const token = await authStorage.getToken();
    recipe['owner_id'] = user.id;

    await fetch(`${HOST}/recipe`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(recipe),
    });

    navigation.navigate('Recipes');
  };

  return (
    <View style={styles.container}>
      <View style={styles.titleBar}>
        <Text style={styles.title}>{recipe.title}</Text>
        <MaterialCommunityIcons
          name="cloud-upload-outline"
          size={35}
          onPress={handleUpload}
        />
      </View>
      <View style={styles.separator} />
      <ScrollView vertical>
        <Text style={styles.subTitle}>Ingredients:</Text>
        <Text>{ingredients}</Text>
        <Text style={styles.subTitle}>Instructions:</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 15,
    height: '100%',
    marginHorizontal: 20,
    padding: 20,
    width: Dimensions.get('window').width * 0.9,
  },
  ingredients: {
    marginVertical: 10,
  },
  instructions: {
    fontSize: 16,
  },
  separator: {
    height: 2,
    backgroundColor: colors.light,
  },
  subTitle: {
    fontWeight: 'bold',
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flexWrap: 'wrap',
  },
  titleBar: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    justifyContent: 'space-between',
  },
});
