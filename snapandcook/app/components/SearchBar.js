import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {StyleSheet, TextInput, View} from 'react-native';

import colors from '../configs/colors';

function SearchBar({search}) {
  return (
    <View style={styles.searchContainer}>
      <Ionicons name="search" size={25} />
      <TextInput
        style={styles.input}
        numberOfLines={1}
        placeholder="search"
        autoCapitalize="none"
        onChangeText={search}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    color: colors.dark,
    flex: 1,
    fontFamily: Platform.OS === 'android' ? 'Roboto' : 'Avenir',
    fontSize: 18,
    marginLeft: 5,
  },
  searchContainer: {
    backgroundColor: colors.white,
    borderRadius: 10,
    flexDirection: 'row',
    marginVertical: 10,
    padding: 5,
    width: '100%',
  },
});

export default SearchBar;
