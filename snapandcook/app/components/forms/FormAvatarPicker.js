import React from 'react';
import {useFormikContext} from 'formik';
import {StyleSheet, View} from 'react-native';

import ErrorMessage from './ErrorMessage';
import AvatarInput from '../AvatarInput';

function FormAvatarPicker({name}) {
  const {errors, setFieldValue, touched, values} = useFormikContext();
  const imageUri = values[name];

  const handleChange = (uri) => {
    setFieldValue(name, uri);
  };

  return (
    <View style={styles.container}>
      <AvatarInput imageUri={imageUri} onChangeImage={handleChange} />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 20,
  },
});

export default FormAvatarPicker;
