export default {
  // primary: '#fc5c65',
  // secondary: '#4ecdc4',
  // primary: '#F65912',
  // secondary: '#3EC5DD',
  primary: '#EC5F57',
  secondary: '#26A9A6',
  black: '#000000',
  white: '#FFFFFF',
  medium: '#6E6969',
  light: '#F8F4F4',
  dark: '#0c0c0c',
  danger: '#FF5252',
};
