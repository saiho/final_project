import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AccountScreen from '../screens/AccountScreen';
import MyListingsScreen from '../screens/MyListingsScreen';
import ListingDetailsScreen from '../screens/ListingDetailsScreen';
import CompletionScreen from '../screens/CompletionScreen';
import MyCompletionsScreen from '../screens/MyCompletionsScreen';

const Stack = createStackNavigator();
const AccountNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name="Account" component={AccountScreen} />
    <Stack.Screen name="My Recipes" component={MyListingsScreen} />
    <Stack.Screen name="My Recipe" component={ListingDetailsScreen} />
    <Stack.Screen name="My Creations" component={MyCompletionsScreen} />
    <Stack.Screen name="My Creation" component={CompletionScreen} />
    {/* <Stack.Screen name="My Recipes" component={MyRecipesScreen} />
    <Stack.Screen name="My Completions" component={MyCompletionsScreen} /> */}
  </Stack.Navigator>
);

export default AccountNavigator;
