import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';

import AccountNavigator from './AccountNavigator';
import BookmarkNavigator from './BookmarkNavigator';
import colors from '../configs/colors';
import CompletionNavigator from './CompletionNavigator';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import FeedNavigator from './FeedNavigator';
import ImagePicker from 'react-native-image-picker';
import NewListingButton from './NewListingButton';
import NewRecipeScreen from '../screens/NewRecipeScreen';

const Tab = createBottomTabNavigator();
const TabNavigator = () => (
  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: colors.primary,
      inactiveTintColor: colors.medium,
    }}>
    <Tab.Screen
      name="Home"
      component={FeedNavigator}
      options={{
        tabBarIcon: ({size, color}) => (
          <MaterialCommunityIcons name="home" size={size} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="Bookmarks"
      component={BookmarkNavigator}
      options={{
        tabBarIcon: ({size, color}) => (
          <MaterialCommunityIcons
            name="bookmark-multiple"
            size={size}
            color={color}
          />
        ),
      }}
    />
    <Tab.Screen
      name="NewRecipeScreen"
      component={NewRecipeScreen}
      options={({navigation}) => ({
        tabBarButton: () => (
          <NewListingButton
            onPress={() => {
              try {
                ImagePicker.showImagePicker(
                  {
                    mediaType: 'photo',
                    quality: 0.6,
                  },
                  (response) => {
                    if (!response.didCancel) {
                      navigation.navigate('NewRecipeScreen', {
                        imageUri: response.uri,
                      });
                    }
                  },
                );
              } catch (error) {
                console.log('Error reading an image', error);
              }
            }}
          />
        ),
      })}
    />
    <Tab.Screen
      name="Creations"
      component={CompletionNavigator}
      options={{
        tabBarIcon: ({size, color}) => (
          <MaterialCommunityIcons name="check-bold" size={size} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="Account"
      component={AccountNavigator}
      options={{
        tabBarIcon: ({size, color}) => (
          <MaterialCommunityIcons name="account" size={size} color={color} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default function AppNavigator() {
  return <TabNavigator />;
}
