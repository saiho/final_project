import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import BookmarksScreen from '../screens/BookmarksScreen';
import CompletionEditScreen from '../screens/CompletionEditScreen';
import CompletionScreen from '../screens/CompletionScreen';
import ListingDetailsScreen from '../screens/ListingDetailsScreen';

const Stack = createStackNavigator();
const BookmarkNavigator = () => (
  <Stack.Navigator mode="modal" headerMode="float">
    <Stack.Screen
      name="Bookmarks"
      component={BookmarksScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen name="Recipe" component={ListingDetailsScreen} />
    <Stack.Screen name="New Creation" component={CompletionEditScreen} />
    <Stack.Screen name="Creation" component={CompletionScreen} />
  </Stack.Navigator>
);

export default BookmarkNavigator;
