import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import CompletionScreen from '../screens/CompletionScreen';
import CompletionsScreen from '../screens/CompletionsScreen';

const Stack = createStackNavigator();
const CompletionNavigator = () => (
  <Stack.Navigator mode="modal" headerMode="float">
    <Stack.Screen
      name="Creations"
      component={CompletionsScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen name="Creation" component={CompletionScreen} />
  </Stack.Navigator>
);

export default CompletionNavigator;
