import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import ListingDetailsScreen from '../screens/ListingDetailsScreen';
import ListingsScreen from '../screens/ListingsScreen';
import CompletionEditScreen from '../screens/CompletionEditScreen';
import CompletionScreen from '../screens/CompletionScreen';

const Stack = createStackNavigator();
const FeedNavigator = () => (
  <Stack.Navigator mode="modal" headerMode="float">
    <Stack.Screen
      name="Recipes"
      component={ListingsScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen name="Recipe" component={ListingDetailsScreen} />
    <Stack.Screen name="New Creation" component={CompletionEditScreen} />
    <Stack.Screen name="Creation" component={CompletionScreen} />
  </Stack.Navigator>
);

export default FeedNavigator;
