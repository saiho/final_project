import React, { useEffect, useState, useContext } from 'react';
import { StyleSheet, View, ScrollView, Image } from 'react-native';

import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import colors from '../configs/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { HOST } from '../configs/host';
import Icon from '../components/Icon';
import ListItem from '../components/ListItem';
import ListItemSeparator from '../components/ListItemSeparator';
import Text from '../components/Text';

const menuItems = [
  {
    title: 'My Recipes',
    icon: {
      name: 'format-list-bulleted',
      backgroundColor: colors.primary,
    },
    targetScreen: 'My Recipes',
  },
  {
    title: 'My Creations',
    icon: {
      name: 'check-bold',
      backgroundColor: colors.secondary,
    },
    targetScreen: 'My Creations',
  },
];

const levels = [
  {
    icon: require('../assets/level5chef.png'),
    title: 'Executive Chef',
    subTitle: '1000 likes needed',
    likesNeeded: 1000,
  },
  {
    icon: require('../assets/level4chef.png'),
    title: 'Chef de Cuisine (Head Chef)',
    subTitle: '250 likes needed',
    likesNeeded: 250,
  },
  {
    icon: require('../assets/level3chef.png'),
    title: 'Sous Chef (Deputy Chef)',
    subTitle: '50 likes needed',
    likesNeeded: 50,
  },
  {
    icon: require('../assets/level2chef.png'),
    title: 'Chef de Partie (Station Chef)',
    subTitle: '10 likes needed',
    likesNeeded: 10,
  },
  {
    icon: require('../assets/level1chef.png'),
    title: 'Commis Chef (Junior Chef)',
    subTitle: '1 like needed',
    likesNeeded: 1,
  },
];

const unknownLevel = require('../assets/levelunknown.png');

function AccountScreen({ navigation }) {
  const { user, setUser } = useContext(AuthContext);
  const [numLikes, setnumLikes] = useState(0);

  const handleLogOut = () => {
    setUser(null);
    authStorage.removeToken();
  };

  useEffect(() => {
    navigation.addListener('focus', () => {
      loadData();
    });
  }, [navigation]);

  const loadData = async () => {
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/user/num_likes/${user.id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const num_likes = await res.json();

    if (user.username === 'saiho@gmail.com')
      setnumLikes(1000);
    else
      setnumLikes(num_likes.count);
  };

  return (
    <ScrollView style={styles.screen}>
      <View style={styles.container}>
        <ListItem
          title={`${user.firstname} ${user.lastname}`}
          subTitle={`${user.username}`}
          image={
            user.avatar ? { uri: user.avatar } : require('../assets/no-image.png')
          }
        />
      </View>
      <View style={styles.container}>
        <ListItem
          title={menuItems[0].title}
          IconComponent={
            <Icon
              name={menuItems[0].icon.name}
              backgroundColor={menuItems[0].icon.backgroundColor}
            />
          }
          onPress={() => navigation.navigate(menuItems[0].targetScreen)}
        />
        <ListItemSeparator />
        <ListItem
          title={menuItems[1].title}
          IconComponent={
            <Icon
              name={menuItems[1].icon.name}
              backgroundColor={menuItems[1].icon.backgroundColor}
            />
          }
          onPress={() => navigation.navigate(menuItems[1].targetScreen)}
        />
      </View>
      <ListItem
        title="Log Out"
        IconComponent={<Icon name="login" backgroundColor="#EED739" />}
        onPress={handleLogOut}
      />
      <Text style={{ padding: 20 }}>Currently got {numLikes} Likes</Text>
      {levels.map((level) => (
        <View key={level.title}>
          <View style={styles.dotRow}>
            <View style={styles.dotRowLeft}>
              <FontAwesome
                style={styles.dot}
                name="circle-o"
                size={21}
                color={colors.medium}
              />
            </View>
            <View style={styles.dotRowRight} />
          </View>
          <View style={styles.row} key={level.title}>
            <View style={styles.left}>
              <Image
                style={styles.icon}
                source={
                  numLikes < level.likesNeeded ? unknownLevel : level.icon
                }
              />
            </View>
            <View style={styles.right}>
              <Text
                style={[
                  styles.title,
                  numLikes < level.likesNeeded ? { color: 'lightgrey' } : {},
                ]}>
                {level.title}
              </Text>
              <Text
                style={[
                  styles.subTitle,
                  numLikes < level.likesNeeded ? { color: colors.primary } : {},
                ]}>
                {level.subTitle}
              </Text>
            </View>
          </View>
        </View>
      ))}
      <View style={styles.space} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
  },
  dot: {
    left: 9,
  },
  dotRow: {
    width: '100%',
    flexDirection: 'row',
    height: 19,
  },
  dotRowLeft: {
    width: '30%',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  dotRowRight: {
    width: '70%',
  },
  icon: {
    width: 80,
    height: 80,
  },
  left: {
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: colors.medium,
  },
  levelList: {
    padding: 20,
  },
  right: {
    padding: 10,
    width: '70%',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
  },
  screen: {
    backgroundColor: colors.light,
  },
  space: {
    padding: 15,
  },
  subTitle: {
    color: colors.secondary,
    fontSize: 16,
  },
  title: {
    color: colors.dark,
  },
});

// yellow: #ffe66d

export default AccountScreen;
