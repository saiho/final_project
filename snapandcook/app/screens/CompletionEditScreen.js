import * as Yup from 'yup';
import React, { useState } from 'react';
import { StyleSheet, KeyboardAvoidingView, ScrollView, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import authStorage from '../auth/store';
import colors from '../configs/colors';
import { ErrorMessage, Form, FormField, FormImagePicker, SubmitButton } from '../components/forms';
import { HOST } from '../configs/host';
import Screen from '../components/Screen';

const validationSchema = Yup.object().shape({
  comment: Yup.string().required().min(1).label('Comment'),
  image: Yup.string().nullable().required().label('Please select an image'),
});

function CompletionEditScreen({ route }) {
  const listing = route.params;
  const navigation = useNavigation();
  const [error, setError] = useState(false);
  const [message, setMessage] = useState('');
  const [isProcessing, setIsProcessing] = useState(false);

  return (
    <Screen style={styles.container}>
      <KeyboardAvoidingView behavior="position">
        <ScrollView>
          <ErrorMessage error={message} visible={error} />
          <Form
            initialValues={{
              comment: '',
              image: null,
            }}
            onSubmit={async (values) => {
              if (isProcessing === true)
                return;
              setIsProcessing(true);
              const data = new FormData();
              data.append('recipe_id', listing.recipe_id);
              data.append('user_id', listing.user_id);
              data.append('comment', values.comment);
              data.append('image', {
                uri: values.image,
                type: 'image/jpg',
                name: 'image.jpg',
              });
              const token = await authStorage.getToken();
              const res = await fetch(`${HOST}/recipe/comment`, {
                method: 'POST',
                headers: {
                  'Authorization': `Bearer ${token}`,
                  'Content-Type': 'multipart/form-data; ',
                },
                body: data,
              });
              setIsProcessing(false);
              if (res.status === 200) {
                navigation.navigate('Recipe');
                return;
              }
              const result = await res.json();
              setError(true);
              setMessage(result.message);
              navigation.navigate('Recipe');
            }}
            validationSchema={validationSchema}>
            <FormImagePicker name="image" />
            <FormField
              maxLength={500}
              multiline
              name="comment"
              numberOfLines={30}
              placeholder="Comment"
            />
            <SubmitButton title="Post" />
          </Form>
          <View style={styles.bottom} />
        </ScrollView>
      </KeyboardAvoidingView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  bottom: {
    padding: Platform.OS === 'ios' ? 60 : 0,
  },
  container: {
    padding: 10,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  photoContainer: {
    alignItems: 'center',
    backgroundColor: colors.light,
    borderRadius: 15,
    height: 300,
    justifyContent: 'center',
    overflow: 'hidden',
    width: '100%',
  },
});

export default CompletionEditScreen;
