import AntDesign from 'react-native-vector-icons/AntDesign';
import {Image, View, ScrollView, Share, StyleSheet} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React, {useContext, useEffect, useState} from 'react';

import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import {captureScreen} from 'react-native-view-shot';
import colors from '../configs/colors';
import {HOST} from '../configs/host';
import Text from '../components/Text';

function CompletionScreen({route}) {
  const completion = route.params;
  const [completor, setCompletor] = useState(null);
  const [like, setLike] = useState(false);
  const [numOfCompletions, setNumOfCompletions] = useState(0);
  const [numOfLikes, setNumOfLikes] = useState(0);
  const {user} = useContext(AuthContext);

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    const token = await authStorage.getToken();
    const res1 = await fetch(`${HOST}/user/${completion.user_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const _completor = await res1.json();
    setCompletor(_completor);

    const res2 = await fetch(
      `${HOST}/recipe/completion/is_like/${completion.id}/${user.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    const {is_like} = await res2.json();
    setLike(is_like);

    const res3 = await fetch(
      `${HOST}/user/num_completions/${completion.user_id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    const num_completions = await res3.json();
    setNumOfCompletions(num_completions.count);

    const res4 = await fetch(
      `${HOST}/recipe/completion/num_likes/${completion.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    const num_likes = await res4.json();
    setNumOfLikes(num_likes.count);
  };

  const handleLike = async () => {
    const token = await authStorage.getToken();
    if (!like) {
      setLike(true);
      await fetch(
        `${HOST}/recipe/completion/like/${completion.id}/${user.id}`,
        {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
    } else {
      setLike(false);
      await fetch(
        `${HOST}/recipe/completion/like/${completion.id}/${user.id}`,
        {
          method: 'DELETE',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
    }

    const res = await fetch(
      `${HOST}/recipe/completion/num_likes/${completion.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    const num_likes = await res.json();
    setNumOfLikes(num_likes.count);
  };

  const handleShare = async () => {
    const base64Data = await captureScreen({
      format: 'jpg',
      quality: 0.6,
      result: 'base64',
    });

    const shareOptions = {
      message: 'A delicious dish from SnapAndCook.',
      // url: `${HOST}/recipe/completion/${completion.id}`,
      type: 'image/jpg',
      url: `data:image/jpg;base64,` + base64Data,
    };
    Share.share(shareOptions);
  };

  return (
    <>
      <Image style={styles.image} source={{uri: completion.picture}} />
      <View style={styles.completorRow}>
        {completor && (
          <>
            <View style={styles.avatarContainer}>
              <Image style={styles.avatar} source={{uri: completor.avatar}} />
            </View>
            <View style={styles.text}>
              <Text>{completor.firstname + ' ' + completor.lastname}</Text>
              <Text style={styles.subtext}>{numOfCompletions} completions</Text>
            </View>
          </>
        )}
        <MaterialCommunityIcons
          name="share-variant"
          size={35}
          color={colors.medium}
          onPress={handleShare}
        />
        <Text style={styles.numOfLikes}>{numOfLikes}</Text>
        {like ? (
          <AntDesign
            style={styles.like}
            name="like1"
            size={35}
            color="dodgerblue"
            onPress={handleLike}
          />
        ) : (
          <AntDesign
            style={styles.like}
            name="like2"
            size={35}
            color={colors.medium}
            onPress={handleLike}
          />
        )}
      </View>
      <ScrollView style={styles.detailsContainer}>
        <Text style={styles.title}>Comment</Text>
        <Text style={styles.comment}>{completion.comment}</Text>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  avatar: {
    height: '100%',
    width: '100%',
  },
  avatarContainer: {
    borderColor: colors.light,
    borderRadius: 25,
    borderWidth: 1,
    height: 50,
    overflow: 'hidden',
    width: 50,
  },
  comment: {
    color: colors.medium,
    fontSize: 17,
    marginVertical: 10,
  },
  completorRow: {
    alignItems: 'center',
    borderBottomColor: colors.light,
    borderBottomWidth: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: '100%',
    height: 300,
  },
  like: {
    marginLeft: 10,
  },
  numOfLikes: {
    marginLeft: 20,
  },
  subtext: {
    color: colors.primary,
    fontSize: 14,
  },
  text: {
    flex: 1,
    marginLeft: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: '500',
  },
  userContainer: {
    marginVertical: 40,
  },
});

export default CompletionScreen;
