import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Image, View, StyleSheet, ScrollView, Share } from 'react-native';
import React, { useState, useContext, useEffect } from 'react';

import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import { captureScreen } from 'react-native-view-shot';
import colors from '../configs/colors';
import GridItems from '../components/GridItems';
import { HOST } from '../configs/host';
import Text from '../components/Text';

function ListingDetailsScreen({ navigation, route }) {
  const [completions, setCompletions] = useState([]);
  const [isCompleted, setIsCompleted] = useState(false);
  const [isBookmarked, setIsBookmarked] = useState(false);
  const recipe = route.params;
  const [numOfRecipes, setNumOfRecipes] = useState(0);
  const { user } = useContext(AuthContext);

  useEffect(() => {
    navigation.addListener('focus', () => {
      loadData();
    });
  }, [navigation]);

  const loadData = async () => {
    const token = await authStorage.getToken();
    const res1 = await fetch(
      `${HOST}/recipe/is_bookmark/${user.id}/${recipe.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    const { is_bookmark } = await res1.json();
    setIsBookmarked(is_bookmark);

    const res2 = await fetch(
      `${HOST}/recipe/is_completed/${user.id}/${recipe.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    const { is_completed } = await res2.json();
    setIsCompleted(is_completed);

    const res3 = await fetch(`${HOST}/recipe/completions/${recipe.id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const completions = await res3.json();
    setCompletions(completions);

    const res4 = await fetch(`${HOST}/user/num_recipes/${recipe.owner_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const num_recipes = await res4.json();
    setNumOfRecipes(num_recipes.count);
  };

  const handleBookmark = async () => {
    const token = await authStorage.getToken();
    if (!isBookmarked) {
      setIsBookmarked(!isBookmarked);
      await fetch(`${HOST}/recipe/bookmark`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_id: user.id,
          recipe_id: recipe.id,
        }),
      });
    } else {
      setIsBookmarked(!isBookmarked);
      await fetch(`${HOST}/recipe/bookmark`, {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_id: user.id,
          recipe_id: recipe.id,
        }),
      });
    }
  };

  const handleComplete = () => {
    if (!isCompleted) {
      navigation.navigate('New Creation', {
        recipe_id: recipe.id,
        user_id: user.id,
      });
    }
  };

  const handleShare = async () => {
    const base64Data = await captureScreen({
      format: 'jpg',
      quality: 0.6,
      result: 'base64',
    });

    const shareOptions = {
      message: 'A delicious dish from SnapAndCook.',
      // url: `${HOST}/recipe/completion/${completion.id}`,
      type: 'image/jpg',
      url: `data:image/jpg;base64,` + base64Data,
    };
    Share.share(shareOptions);
  };

  return (
    <ScrollView>
      <Image style={styles.image} source={{ uri: recipe.picture }} />
      <View style={styles.buttons}>
        <View style={styles.avatarContainer}>
          <Image style={styles.avatar} source={{ uri: recipe.avatar }} />
        </View>
        <View style={styles.text}>
          <Text>{recipe.firstname + ' ' + recipe.lastname}</Text>
          <Text style={styles.subtext}>{numOfRecipes} Recipes</Text>
        </View>
        <MaterialCommunityIcons
          name="share-variant"
          size={35}
          color={colors.medium}
          onPress={handleShare}
          style={styles.sharebtn}
        />
        {isBookmarked ? (
          <MaterialCommunityIcons
            name="bookmark"
            size={35}
            color={colors.primary}
            onPress={handleBookmark}
          />
        ) : (
            <MaterialCommunityIcons
              name="bookmark-outline"
              size={35}
              color={colors.medium}
              onPress={handleBookmark}
            />
          )}
        {isCompleted ? (
          <MaterialCommunityIcons
            style={styles.complete}
            name="check-circle"
            size={35}
            color={colors.secondary}
            onPress={handleComplete}
          />
        ) : (
            <MaterialCommunityIcons
              style={styles.complete}
              name="check-circle-outline"
              size={35}
              color={colors.medium}
              onPress={handleComplete}
            />
          )}
      </View>
      <View style={styles.detailsContainer}>
        <Text style={styles.title}>{recipe.title}</Text>
        <Text style={styles.content}>{recipe.content}</Text>
        {!completions[0] && <View style={styles.extraPadding} />}
        {completions[0] && <GridItems items={completions} />}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  avatar: {
    width: '100%',
    height: '100%',
  },
  avatarContainer: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderColor: colors.light,
    borderWidth: 1,
    overflow: 'hidden',
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomColor: colors.light,
    borderBottomWidth: 1,
  },
  content: {
    color: colors.medium,
    fontSize: 17,
    marginVertical: 10,
  },
  detailsContainer: {
    padding: 20,
  },
  extraPadding: {
    marginBottom: 50,
  },
  image: {
    width: '100%',
    height: 300,
  },
  complete: {
    marginLeft: 10,
  },
  sharebtn: {
    marginRight: 10,
  },
  subtext: {
    fontSize: 14,
    color: colors.primary,
  },
  text: {
    flex: 1,
    marginLeft: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: '500',
  },
  userContainer: {
    marginVertical: 40,
  },
});

export default ListingDetailsScreen;
