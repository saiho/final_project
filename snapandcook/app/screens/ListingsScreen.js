import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet} from 'react-native';

import ActivityIndicator from '../components/ActivityIndicator';
import authStorage from '../auth/store';
import Button from '../components/Button';
import Card from '../components/Card';
import colors from '../configs/colors';
import {convertTime} from '../utils/time';
import {HOST} from '../configs/host';
import Screen from '../components/Screen';
import SearchBar from '../components/SearchBar';
import SwitchSelector from 'react-native-switch-selector';
import Text from '../components/Text';

function ListingsScreen({navigation}) {
  const [error, setError] = useState(false);
  const [isLatest, setIsLatest] = useState(true);
  const [loading, setLoading] = useState(false);
  const [recipes, setRecipes] = useState([]);

  const handleSearch = async (text) => {
    setLoading(true);
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/recipes/search`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({query: text}),
    });
    let data = await res.json();
    if (res.status !== 200) {
      setError(true);
      return;
    }
    isLatest ? sortByLatest(data) : sortByMostCompletions(data);
    setError(false);
    setLoading(false);
    setRecipes(data);
  };

  const loadRecipes = async () => {
    setLoading(true);
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/recipes`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await res.json();
    if (res.status !== 200) {
      setError(true);
      return;
    }
    isLatest ? sortByLatest(data) : sortByMostCompletions(data);
    setError(false);
    setLoading(false);
    setRecipes(data);
  };

  const sort = (value) => {
    let data = [...recipes];
    setIsLatest(value);
    if (value) {
      sortByLatest(data);
    } else {
      sortByMostCompletions(data);
    }
    setRecipes(data);
  };

  const sortByMostCompletions = (data) => {
    data.sort((a, b) => {
      return parseInt(b.num_completions) - parseInt(a.num_completions);
    });
  };

  const sortByLatest = (data) => {
    data.sort((a, b) => {
      const date1 = new Date(a.created_at);
      const date2 = new Date(b.created_at);
      return parseInt(date2 - date1);
    });
  };

  useEffect(() => {
    navigation.addListener('focus', () => {
      loadRecipes();
    });
  }, [navigation]);

  // useEffect(() => {
  //   loadRecipes();
  // }, []);

  return (
    <Screen style={styles.screen}>
      {error && (
        <>
          <Text>Couldn't retrieve the listings.</Text>
          <Button title="Retry" onPress={loadRecipes}></Button>
        </>
      )}
      <SearchBar search={handleSearch} />
      <SwitchSelector
        initial={0}
        onPress={sort}
        style={styles.selector}
        textColor={colors.primary}
        selectedColor={colors.white}
        buttonColor={colors.primary}
        borderColor={colors.primary}
        backgroundColor={colors.light}
        hasPadding
        options={[
          {label: 'Latest', value: true},
          {label: 'Most Creations', value: false},
        ]}
      />
      <ActivityIndicator visible={loading} />
      <FlatList
        data={recipes}
        keyExtractor={(recipe) => recipe.id.toString()}
        renderItem={({item}) => {
          return (
            <Card
              title={item.title}
              subTitle={
                `${item.num_completions} Creations ` +
                `${convertTime(item.created_at)}`
              }
              image={{uri: item.picture}}
              onPress={() => navigation.navigate('Recipe', item)}
            />
          );
        }}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: colors.light,
    paddingHorizontal: Platform.OS === 'ios' ? 20 : 10,
  },
  selector: {
    marginBottom: 10,
  },
});

export default ListingsScreen;
