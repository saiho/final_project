import React, {useState, useContext} from 'react';
import {StyleSheet} from 'react-native';
import * as Yup from 'yup';

import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import colors from '../configs/colors';
import {ErrorMessage, Form, FormField, SubmitButton} from '../components/forms';
import {HOST} from '../configs/host';
import Screen from '../components/Screen';

const validationSchema = Yup.object().shape({
  email: Yup.string().required().email().label('Email'),
  password: Yup.string().required().min(4).label('Password'),
});

export default function LoginScreen() {
  const authContext = useContext(AuthContext);
  const [loginFailed, setLoginFailed] = useState(false);

  const handleLogin = async ({email, password}) => {
    const loginRes = await fetch(`${HOST}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: email,
        password: password,
      }),
    });

    if (loginRes.status !== 200) return setLoginFailed(true);

    setLoginFailed(false);
    const {token} = await loginRes.json();

    const userRes = await fetch(`${HOST}/current_user`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (userRes.status !== 200) return setLoginFailed(true);

    const user = await userRes.json();
    authContext.setUser(user);
    authStorage.storeToken(token);
  };

  return (
    <Screen style={styles.container}>
      {/* <Image style={styles.logo} source={require('../assets/logo2.png')} /> */}
      <Form
        initialValues={{email: '', password: ''}}
        onSubmit={(values) => handleLogin(values)}
        validationSchema={validationSchema}>
        <ErrorMessage
          error="Invalid email or password."
          visible={loginFailed}
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="email"
          keyboardType="email-address"
          name="email"
          placeholder="Email"
          textContentType="emailAddress"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="lock"
          name="password"
          placeholder="Password"
          secureTextEntry
          textContentType="password"
        />
        <SubmitButton title="Login" />
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  logo: {
    alignSelf: 'center',
    height: 120,
    marginBottom: 20,
    marginTop: 50,
    width: 120,
    tintColor: colors.primary,
  },
});
