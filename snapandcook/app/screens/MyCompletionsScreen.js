import React, {useEffect, useState, useContext} from 'react';
import {FlatList, StyleSheet} from 'react-native';

import ActivityIndicator from '../components/ActivityIndicator';
import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import Button from '../components/Button';
import Card from '../components/Card';
import colors from '../configs/colors';
import {convertTime} from '../utils/time';
import {HOST} from '../configs/host';
import Screen from '../components/Screen';
import SearchBar from '../components/SearchBar';
import SwitchSelector from 'react-native-switch-selector';
import Text from '../components/Text';

function MyCompletionsScreen({navigation}) {
  const [completions, setCompletions] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isLatest, setIsLatest] = useState(true);
  const {user} = useContext(AuthContext);

  const handleSearch = async (text) => {
    setLoading(true);
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/completions/search`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({query: text}),
    });
    let data = await res.json();
    if (res.status !== 200) {
      setError(true);
      return;
    }
    data = data.filter((d) => d.user_id === user.id);
    setError(false);
    setLoading(false);
    isLatest ? sortByLatest(data) : sortByMostLikes(data);
    setCompletions(data);
  };

  const loadData = async () => {
    setLoading(true);
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/recipe/completions`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    let data = await res.json();
    if (res.status !== 200) {
      setError(true);
      return;
    }
    data = data.filter((d) => d.user_id === user.id);
    setError(false);
    setLoading(false);
    setCompletions(data);
  };

  const sort = (value) => {
    const data = [...completions];
    setIsLatest(value);
    if (value) {
      sortByLatest(data);
    } else {
      sortByMostLikes(data);
    }
    setCompletions(data);
  };

  const sortByMostLikes = (data) => {
    data.sort((a, b) => {
      return parseInt(b.num_likes) - parseInt(a.num_likes);
    });
  };

  const sortByLatest = (data) => {
    data.sort((a, b) => {
      const date1 = new Date(a.created_at);
      const date2 = new Date(b.created_at);
      return parseInt(date2 - date1);
    });
  };

  useEffect(() => {
    navigation.addListener('focus', () => {
      loadData();
    });
  }, [navigation]);

  // useEffect(() => {
  //   loadData();
  // }, []);

  return (
    <Screen style={styles.screen}>
      {error && (
        <>
          <Text>Couldn't retrieve the listings.</Text>
          <Button title="Retry" onPress={loadCompletions}></Button>
        </>
      )}
      <SearchBar search={handleSearch} />
      <SwitchSelector
        initial={0}
        onPress={sort}
        style={styles.selector}
        textColor={colors.primary}
        selectedColor={colors.white}
        buttonColor={colors.primary}
        borderColor={colors.primary}
        backgroundColor={colors.light}
        hasPadding
        options={[
          {label: 'Latest', value: true},
          {label: 'Most Likes', value: false},
        ]}
      />
      {!completions[0] && <Text>No creations found!</Text>}
      <ActivityIndicator visible={loading} />
      <FlatList
        data={completions}
        keyExtractor={(completion) => completion.id.toString()}
        renderItem={({item}) => {
          return (
            <Card
              title={item.title}
              subTitle={`${item.num_likes} Likes ${convertTime(
                item.created_at,
              )}`}
              image={{uri: item.picture}}
              onPress={() => navigation.navigate('My Creation', item)}
            />
          );
        }}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 10,
    backgroundColor: colors.light,
  },
  selector: {
    marginBottom: 10,
  },
});

export default MyCompletionsScreen;
