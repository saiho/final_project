import {FlatList, StyleSheet} from 'react-native';
import React, {useEffect, useState, useContext} from 'react';

import ActivityIndicator from '../components/ActivityIndicator';
import AuthContext from '../auth/context';
import authStorage from '../auth/store';
import Button from '../components/Button';
import Card from '../components/Card';
import colors from '../configs/colors';
import {convertTime} from '../utils/time';
import {HOST} from '../configs/host';
import Screen from '../components/Screen';
import SearchBar from '../components/SearchBar';
import SwitchSelector from 'react-native-switch-selector';
import Text from '../components/Text';

function MyListingsScreen({navigation}) {
  const [error, setError] = useState(false);
  const [isLatest, setIsLatest] = useState(true);
  const [loading, setLoading] = useState(false);
  const [recipes, setRecipes] = useState([]);
  const {user} = useContext(AuthContext);

  const handleSearch = async (text) => {
    setLoading(true);
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/recipes/search`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({query: text}),
    });
    let data = await res.json();
    data = data.filter((d) => d.owner_id === user.id);
    if (res.status !== 200) {
      setError(true);
      return;
    }
    setError(false);
    setLoading(false);
    isLatest ? sortByLatest(data) : sortByMostCompletions(data);
    setRecipes(data);
  };

  const loadRecipes = async () => {
    setLoading(true);
    const token = await authStorage.getToken();
    const res = await fetch(`${HOST}/recipes`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    let data = await res.json();
    if (res.status !== 200) {
      setError(true);
      return;
    }
    data = data.filter((d) => d.owner_id === user.id);
    setError(false);
    setLoading(false);
    setRecipes(data);
  };

  const sort = (value) => {
    let data = [...recipes];
    setIsLatest(value);
    if (value) {
      sortByLatest(data);
    } else {
      sortByMostCompletions(data);
    }
    setRecipes(data);
  };

  const sortByMostCompletions = (data) => {
    data.sort((a, b) => {
      return parseInt(b.num_completions) - parseInt(a.num_completions);
    });
  };

  const sortByLatest = (data) => {
    data.sort((a, b) => {
      const date1 = new Date(a.created_at);
      const date2 = new Date(b.created_at);
      return parseInt(date2 - date1);
    });
  };

  useEffect(() => {
    navigation.addListener('focus', () => {
      loadRecipes();
    });
  }, [navigation]);

  // useEffect(() => {
  //   loadRecipes();
  // }, []);

  return (
    <Screen style={styles.screen}>
      {error && (
        <>
          <Text>Couldn't retrieve the listings.</Text>
          <Button title="Retry" onPress={loadRecipes}></Button>
        </>
      )}
      <SearchBar search={handleSearch} />
      <SwitchSelector
        initial={0}
        onPress={sort}
        style={styles.selector}
        textColor={colors.primary}
        selectedColor={colors.white}
        buttonColor={colors.primary}
        borderColor={colors.primary}
        backgroundColor={colors.light}
        hasPadding
        options={[
          {label: 'Latest', value: true},
          {label: 'Most Completions', value: false},
        ]}
      />
      {!recipes[0] && <Text>My recipes not found!</Text>}
      <ActivityIndicator visible={loading} />
      <FlatList
        data={recipes}
        keyExtractor={(recipe) => recipe.id.toString()}
        renderItem={({item}) => {
          return (
            <Card
              title={item.title}
              subTitle={
                `${item.num_completions} Completions ` +
                `${convertTime(item.created_at)}`
              }
              image={{uri: item.picture}}
              onPress={() => navigation.navigate('My Recipe', item)}
            />
          );
        }}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 10,
    backgroundColor: colors.light,
  },
  selector: {
    marginBottom: 10,
  },
});

export default MyListingsScreen;
