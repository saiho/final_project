import React, {useEffect, useState} from 'react';

import authStorage from '../auth/store';
import ActivityIndicator from '../components/ActivityIndicator';
import {HOST} from '../configs/host';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import RecipeCard from '../components/RecipeCard';

export default function NewRecipeScreen({route}) {
  const [loading, setLoading] = useState(false);
  const [recipes, setRecipes] = useState([]);

  const {imageUri} = route.params;

  const getRecipes = async () => {
    setLoading(true);
    const token = await authStorage.getToken();
    const data = new FormData();

    data.append('image', {
      uri: imageUri,
      type: 'image/jpg',
      name: `image.jpg`,
    });

    const res = await fetch(`${HOST}/recipe/generate`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data; ',
        // Accept: 'application/x-www-form-urlencoded',
      },
      body: data,
    });

    const result = await res.json();
    setLoading(false);
    setRecipes(result);
  };

  useEffect(() => {
    getRecipes();
  }, [route]);

  if (loading) return <ActivityIndicator visible={loading} isRecipe={true} />;
  else
    return (
      <>
        <View style={styles.container}>
          <Image style={{flex: 1}} source={{uri: imageUri}} />
        </View>
        <ScrollView style={styles.cards} horizontal>
          {recipes.map((recipe, idx) => (
            <RecipeCard key={`recipe_${idx}`} recipe={recipe} />
          ))}
        </ScrollView>
      </>
    );
}

const styles = StyleSheet.create({
  cards: {
    bottom: 40,
    height: '50%',
    position: 'absolute',
  },
  container: {
    flex: 1,
  },
});
