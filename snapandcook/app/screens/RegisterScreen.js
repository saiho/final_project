import React, { useState } from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Yup from 'yup';
import { HOST } from '../configs/host';

import Screen from '../components/Screen';
import {
  ErrorMessage,
  Form,
  FormAvatarPicker,
  FormField,
  SubmitButton,
} from '../components/forms';

const validationSchema = Yup.object().shape({
  firstname: Yup.string().required().label('First Name'),
  lastname: Yup.string().required().label('Last Name'),
  email: Yup.string().required().email().label('Email'),
  password: Yup.string().required().min(4).label('Password'),
});

export default function RegisterScreen() {
  const [error, setError] = useState(false);
  const [message, setMessage] = useState('');
  const [isProcessing, setIsProcessing] = useState(false);
  const navigation = useNavigation();

  return (
    <Screen style={styles.container}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView behavior="position">
          <ScrollView>
            <ErrorMessage error={message} visible={error} />
            <Form
              initialValues={{
                avatar: null,
                firstname: '',
                lastname: '',
                email: '',
                password: '',
              }}
              onSubmit={async (values) => {
                if (isProcessing === true)
                  return;
                setIsProcessing(true);
                const data = new FormData();
                data.append('firstname', values.firstname);
                data.append('lastname', values.lastname);
                data.append('email', values.email);
                data.append('password', values.password);
                if (values.avatar)
                  data.append('avatar', {
                    uri: values.avatar,
                    type: 'image/jpg',
                    name: `avatar.jpg`,
                  });

                const res = await fetch(`${HOST}/register`, {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'multipart/form-data; ',
                  },
                  body: data,
                });

                setIsProcessing(false);
                if (res.status === 200) {
                  navigation.navigate('Welcome');
                  return;
                }
                const result = await res.json();
                setError(true);
                setMessage(result.message);
              }}
              validationSchema={validationSchema}>
              <FormAvatarPicker name="avatar" />
              <FormField
                autoCorrect={false}
                icon="account"
                keyboardType="default"
                name="firstname"
                placeholder="First Name"
              />
              <FormField
                autoCorrect={false}
                icon="account"
                keyboardType="default"
                name="lastname"
                placeholder="Last Name"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="email"
                keyboardType="email-address"
                name="email"
                placeholder="Email"
                textContentType="emailAddress"
              />
              <FormField
                autoCapitalize="none"
                autoCorrect={false}
                icon="lock"
                name="password"
                placeholder="Password"
                secureTextEntry
                textContentType="password"
              />
              <SubmitButton title="Register" />
            </Form>
            <View style={styles.bottom} />
          </ScrollView>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
  },
  bottom: {
    padding: 45,
  },
});
