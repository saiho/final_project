import {ImageBackground, View, StyleSheet, Image} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

import Button from '../components/Button';
import colors from '../configs/colors';
import Text from '../components/Text';

function WelcomeScreen(props) {
  const navigation = useNavigation();
  return (
    <ImageBackground
      blurRadius={5}
      style={styles.background}
      source={require('../assets/bg.jpeg')}>
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require('../assets/logo3.png')} />
        <Text style={styles.tagline}>Snap and Cook</Text>
        <Text style={styles.tagline}>Get Inspired to cook!</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <Button
          title="login"
          onPress={() => {
            navigation.navigate('Login');
          }}
        />
        <Button
          title="Register"
          color="secondary"
          onPress={() => {
            navigation.navigate('Register');
          }}
        />
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonsContainer: {
    width: '100%',
    padding: 20,
  },
  logo: {
    width: 140,
    height: 140,
    // tintColor: 'rgba(255, 255, 255, 0.7)',
  },
  logoContainer: {
    position: 'absolute',
    top: 70,
    alignItems: 'center',
  },
  tagline: {
    // color: 'rgba(255, 255, 255, 0.9)',
    color: colors.secondary,
    fontSize: 25,
    fontWeight: '600',
    paddingVertical: 10,
  },
});
export default WelcomeScreen;
