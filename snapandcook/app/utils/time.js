export function convertTime(timestamp) {
  const createdAt = new Date(timestamp);
  const now = new Date();
  const diff = (now - createdAt) / 1000;

  if (diff / 60 < 1) {
    return Math.floor(diff) + ' seconds ago';
  } else if (diff / (60 * 60) < 1) {
    return Math.floor(diff / 60) + ' minutes ago';
  } else if (diff / (60 * 60 * 24) < 1) {
    return Math.floor(diff / (60 * 60)) + ' hours ago';
  } else if (diff / (60 * 60 * 24 * 30) < 1) {
    return Math.floor(diff / (60 * 60 * 24)) + ' days ago';
  } else if (diff / (60 * 60 * 24 * 30 * 12) < 1) {
    return Math.floor(diff / (60 * 60 * 24 * 30)) + ' months ago';
  } else {
    return Math.floor(diff / (60 * 60 * 24 * 30 * 12)) + ' years ago';
  }
}
